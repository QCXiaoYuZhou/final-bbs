//package com.bbs.controller;
//
//import com.bbs.entity.Post;
//import com.bbs.service.PostService;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.*;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@ExtendWith(MockitoExtension.class)
//class PostControllerTest {
//
//    private MockMvc mockMvc;
//
//    @Mock
//    private PostService postService;
//
//    @InjectMocks // Automatically inject the mocks by type
//    private PostController postController;
//
//    @BeforeEach
//    void setUp() {
//        mockMvc = MockMvcBuilders.standaloneSetup(postController).build();
//    }
//
//    @Test
//    void createPost_Success() throws Exception {
//        Post post = new Post();
//        post.setTitle("Test Post");
//        post.setContent("This is a test post content.");
//
//        when(postService.createPost(any(Post.class))).thenReturn(post);
//
//        mockMvc.perform(post("/api/posts/create")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content("{\"title\":\"Test Post\",\"content\":\"This is a test post content.\"}"))
//                .andExpect(status().isOk());
//
//        verify(postService, times(1)).createPost(any(Post.class));
//    }
//
//    @Test
//    void createPost_Failure() throws Exception {
//        when(postService.createPost(any(Post.class))).thenReturn(null);
//
//        mockMvc.perform(post("/api/posts/create")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content("{\"title\":\"Test Post\",\"content\":\"This is a test post content.\"}"))
//                .andExpect(status().isOk());
//
//        verify(postService, times(1)).createPost(any(Post.class));
//    }
//}

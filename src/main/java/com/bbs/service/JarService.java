package com.bbs.service;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Service
public class JarService {

    public void runJar() throws Exception {
        ProcessBuilder builder = new ProcessBuilder(
//                "java", "-jar", "D:\\ZhuoMian\\MyGame\\final\\bbs\\src\\main\\resources\\lb-1.0-jar-with-dependencies.jar");
                "java", "-jar", "src/main/resources/lb-1.0-jar-with-dependencies.jar");
        builder.redirectErrorStream(true);
        Process process = builder.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }

        int exitCode = process.waitFor();
        System.out.println("Exited with code : " + exitCode);
    }
}

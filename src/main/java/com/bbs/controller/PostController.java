package com.bbs.controller;

import com.bbs.aop.LogAnnotation;
import com.bbs.entity.Comment;
import com.bbs.entity.Post;
import com.bbs.service.ConfigurationService;
import com.bbs.service.PostService;
import com.bbs.util.R;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/api/posts")
public class PostController {
    @Resource
    private PostService postService;
    @Resource
    private ConfigurationService configurationService;

    @LogAnnotation("发布帖子") // 被观察者
//    @PreAuthorize("authenticated") "request"中已经包含了用户信息，无需再次验证
    @PostMapping("/create")
    public R<?> createPost(@RequestBody Post post) {
        postService.createPost(post);
        return R.ok(null);
    }

    @LogAnnotation("获取所有已审核的帖子")
    @GetMapping("/reviewed")
    public R<?> getAllReviewedPosts(@RequestParam(defaultValue = "1") int page,
                                    @RequestParam(defaultValue = "10") int limit) {
        Page<Post> posts = postService.getAllReviewedPosts(page - 1, limit);
        return R.ok(Map.of(
                "total", posts.getTotalElements(),
                "posts", posts.getContent()
        ));
    }

    @LogAnnotation("管理员获取所有未审核的帖子")
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/unreviewed")
    public R<?> getAllUnreviewedPosts(@RequestParam(defaultValue = "1") int page,
                                      @RequestParam(defaultValue = "10") int limit) {
        Page<Post> posts = postService.getAllUnreviewedPosts(page - 1, limit);
        return R.ok(Map.of(
                "total", posts.getTotalElements(),
                "posts", posts.getContent()
        ));
    }

    @LogAnnotation("查看帖子详情")
    @GetMapping("/{postId}")
    public R<?> getPost(@PathVariable Long postId) {
        Post post = postService.getPost(postId);
        return R.ok(post);
    }

    @LogAnnotation("编辑帖子")
    @PutMapping("/{postId}")
    public R<?> updatePost(@PathVariable Long postId,
                           @RequestBody Map<String, String> updates) {
        postService.updatePost(postId, updates.get("title"), updates.get("content"));
        return R.ok(null);
    }

    @LogAnnotation("删除帖子")
    @DeleteMapping("/{postId}")
    public R<?> deletePost(@PathVariable Long postId) {
        postService.deletePost(postId);
        return R.ok(null);
    }

//    @LogAnnotation("上传帖子的媒体文件")
//    @PreAuthorize("authenticated")
//    @PostMapping("/{postId}/media")
//    public R<?> uploadMedia(@PathVariable Long postId,
//                            @RequestParam("media") MultipartFile[] files) throws IOException {
//        List<String> mediaUrls = new ArrayList<>();
//        for (MultipartFile file : files) {
//            String fileUrl = mediaService.saveFile(file);
//            mediaUrls.add(fileUrl);
//        }
//
//        mediaService.addMediaToPost(postId, mediaUrls);
//
//        return R.ok(mediaUrls);
//    }

    @LogAnnotation("设置帖子审核开关")
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/review/switch")
    public R<?> setReviewSwitch(@RequestBody Map<String, Boolean> payload) {
        configurationService.setReviewSwitch(payload.get("enabled"));
        return R.ok(null);
    }

    @LogAnnotation("审核帖子")
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{postId}/review")
    public R<?> reviewPost(@PathVariable Long postId,
                           @RequestBody Map<String, Boolean> payload) {
        postService.setPostReview(postId, payload.get("approved"));
        return R.ok(null);
    }


    // 互动交流模块
    @LogAnnotation("添加评论")
    @PostMapping("/{postId}/comments")
    public R<?> addComment(@PathVariable Long postId,
                           @RequestBody Comment comment) {
        postService.addComment(postId, comment);
        return R.ok(null);
    }

    @LogAnnotation("删除评论")
    @DeleteMapping("/comments/{commentId}")
    public R<?> deleteComment(@PathVariable Long commentId) {
        postService.deleteComment(commentId);
        return R.ok(null);
    }

    @LogAnnotation("点赞/取消点赞帖子")
    @PostMapping("/{postId}/like")
    public R<?> likePost(@PathVariable Long postId) {
        postService.likePost(postId);
        return R.ok(null);
    }

    @LogAnnotation("收藏/取消收藏帖子")
    @PostMapping("/{postId}/favorite")
    public R<?> favoritePost(@PathVariable Long postId) {
        postService.favoritePost(postId);
        return R.ok(null);
    }

}

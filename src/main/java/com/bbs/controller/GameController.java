package com.bbs.controller;

import com.bbs.service.JarService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/game")
public class GameController {
    private final JarService jarService;

    public GameController(JarService jarService) {
        this.jarService = jarService;
    }

    @GetMapping("/lb")
    public void lb() {
        System.out.println("请求到达 /lb");
        try {
            jarService.runJar();
        } catch (Exception e) {
            System.out.println("出现了异常:" + e.getMessage());
            e.printStackTrace();  // 打印堆栈跟踪，以便更好地理解错误原因
        }
    }
}

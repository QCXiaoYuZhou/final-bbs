package com.bbs.controller;

import com.bbs.aop.LogAnnotation;
import com.bbs.entity.User;
import com.bbs.service.UserService;
import com.bbs.util.R;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Resource
    private UserService userService;

    @LogAnnotation("获取用户信息")
    @GetMapping("/profile")
    public R<?> getUserProfile() {
        User user = userService.getCurrentUser();
        return R.ok(Map.of(
                "id", user.getId(),
                "email", user.getEmail(),
                "avatar", user.getAvatar(),
                "nickname", user.getNickname(),
                "signature", user.getSignature(),
                "role", user.getRole().getName()
        ));
    }

    @LogAnnotation("更新用户信息")
    @PutMapping("/profile")
    public R<?> updateUserProfile(@RequestBody User updatedUser) {
        userService.update(updatedUser);
        return R.ok(null);
    }

}

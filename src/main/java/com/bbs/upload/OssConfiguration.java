package com.bbs.upload;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 阿里云OSS配置类,用于创建AliOssUtil对象
 */
@Configuration
@Slf4j
public class OssConfiguration {

    @Bean// 项目启动后自动调用该方法创建AliOssUtil对象,并将该对象交给Spring容器管理
    @ConditionalOnMissingBean// 当Spring容器中没有AliOssUtil对象时才创建
    public AliOssUtil aliOssUtil(AliOssProperties aliOssProperties) {
        log.info("创建AliOssUtil对象:{}", aliOssProperties);
        return new AliOssUtil(
                aliOssProperties.getEndpoint(),
                aliOssProperties.getAccessKeyId(),
                aliOssProperties.getAccessKeySecret(),
                aliOssProperties.getBucketName()
        );
    }

}

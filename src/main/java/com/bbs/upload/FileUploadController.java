//package com.bbs.upload;// FileUploadController.java
//
//import com.bbs.util.R;
//import org.apache.commons.io.FilenameUtils;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.*;
//
//@RestController
//@RequestMapping("/api")
//public class FileUploadController {
//
//    @PostMapping("/upload")
//    public R<?> handleFileUpload(@RequestParam("files") MultipartFile[] files) {
//        List<String> fileUrls = new ArrayList<>();
//        String uploadDirectory = "E:\\upload"; // Windows路径
//
//        for (MultipartFile file : files) {
//            if (file.isEmpty()) {
//                return R.fail("文件不能为空");
//            }
//
//            // 生成文件名
//            String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
//            String fileName = UUID.randomUUID() + "." + fileExtension;
//
//            // 保存文件
//            try {
//                File destinationFile = new File(uploadDirectory + File.separator + fileName);
//                file.transferTo(destinationFile);
//
//                // 返回文件访问URL
//                String fileAccessUrl = "http://localhost:8090/files/" + fileName;
//                fileUrls.add(fileAccessUrl);
//            } catch (IOException e) {
//                e.printStackTrace();
//                return R.fail("文件上传失败");
//            }
//        }
//
//        Map<String, List<String>> data = new HashMap<>();
//        data.put("urls", fileUrls);
//        return R.ok(data);
//    }
//}

package com.bbs.upload;// WebConfig.java

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/files/**").addResourceLocations("file:/path/to/upload/directory/");
        // 注意这里路径的写法，使用file:前缀和三个斜杠
        registry.addResourceHandler("/files/**").addResourceLocations("file:///E:/upload/");
    }
}

package com.bbs.lb.main;


import com.bbs.lb.other.World;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame("CarrotFantasy");
        World world = new World();
        frame.add(world);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(960, 640);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        world.action();
    }
}

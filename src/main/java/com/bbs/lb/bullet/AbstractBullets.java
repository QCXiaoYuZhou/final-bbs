package com.bbs.lb.bullet;

import lombok.Getter;
import lombok.Setter;

import java.awt.*;

@Setter
@Getter
public abstract class AbstractBullets {
    public int width;
    public int height;
    protected int x;
    protected int y;
    protected double angle;
    protected int speed;
    protected int level;
    protected int canNotShot;
    protected boolean outOfDistance;
    protected int flyDistance;
    protected int atk;
    protected boolean startBoom;

    public AbstractBullets(int x, int y, double angrad, int width, int height, int speed, int level, int canNotShot, int atk) {
        this.x = x;
        this.y = y;
        this.angle = angrad;
        this.width = width;
        this.height = height;
        this.speed = speed;
        this.level = level;
        this.canNotShot = canNotShot;
        this.outOfDistance = false;
        this.flyDistance = 0;
        this.atk = atk;
        this.startBoom = false;
    }

    public abstract void step();

    public abstract void paint(Graphics g);
}

package com.bbs.lb.bullet;

import com.bbs.lb.other.GameTools;
import lombok.Getter;
import lombok.Setter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

@Getter
@Setter
public class BulletOfBlueStar extends AbstractBullets {
    private static final BufferedImage[] images = new BufferedImage[3];

    static {
        try {
            for (int i = 0; i < images.length; i++) {
                images[i] = ImageIO.read(BulletOfGreen.class.getResource("/png/BulletOfTStar" + i + ".png"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean boom;

    public BulletOfBlueStar(int x, int y, double angrad, int level, int cannon_shot, int ATK) {
        super(x, y, angrad, 20, 20, 25, level, cannon_shot, ATK);
        this.atk = ATK;
        boom = false;
    }

    public BufferedImage getImage() {
        if (boom) {
            return null;
        }
        return images[level];
    }

    //斜着飞
    public void step() {
        GameTools.getFlyingLine(this);
        flyDistance += speed;
        outOfDistance = flyDistance >= canNotShot;
    }

    public void paint(Graphics g) {
        step();
        g.drawImage(getImage(), this.x, this.y, null);
    }
}

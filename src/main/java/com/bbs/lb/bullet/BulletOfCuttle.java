package com.bbs.lb.bullet;

import com.bbs.lb.other.GameTools;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class BulletOfCuttle extends AbstractBullets {

    private static BufferedImage images;

    static {
        try {
            images = ImageIO.read(BulletOfCuttle.class.getResource("/png/BulletOfCuttle.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BulletOfCuttle(int x, int y, double angrad, int level, int cannon_shot, int ATK) {
        super(x, y, angrad, 20, 20, 15, level, cannon_shot, ATK);
    }

    public BufferedImage getImage() {
        return images;
    }

    @Override
    public void step() {
        GameTools.getFlyingLine(this);
        flyDistance += speed;
        if (flyDistance >= canNotShot) {
            outOfDistance = true;
        } else {
            outOfDistance = false;
        }
    }

    @Override
    public void paint(Graphics g) {
        Graphics g2 = g.create();
        step();
        g2.drawImage(getImage(), this.x, this.y, null);
    }

}

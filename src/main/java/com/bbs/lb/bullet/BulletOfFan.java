package com.bbs.lb.bullet;

import com.bbs.lb.other.GameTools;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;


public class BulletOfFan extends AbstractBullets {
    private static BufferedImage[] images = new BufferedImage[3];

    static {

        try {
            for (int i = 0; i < images.length; i++) {
                images[i] = ImageIO.read(BulletOfGreen.class.getResource("/png/BulletOfFan" + i + ".png"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    int index = 0;

    public BulletOfFan(int x, int y, double angrad, int level, int canNotShot, int atk) {
        super(x, y, angrad, 20, 20, 22, level, canNotShot, atk);
        this.atk = atk;
    }

    public BufferedImage getImage() {
        return images[level];

    }

    @Override
    public void step() {
        GameTools.getFlyingLine(this);
        flyDistance += speed;
        if (flyDistance >= canNotShot) {
            outOfDistance = true;
        } else {
            outOfDistance = false;
        }
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g.create();
        step();
        g2.rotate(index++, this.x + this.width / 2, this.y + this.height / 2);
        g2.drawImage(getImage(), this.x, this.y, null);
    }
}

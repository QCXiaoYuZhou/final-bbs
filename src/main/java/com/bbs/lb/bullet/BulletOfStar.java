package com.bbs.lb.bullet;

import com.bbs.lb.other.GameTools;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class BulletOfStar extends AbstractBullets {
    private static BufferedImage[] images = new BufferedImage[3];

    static {

        try {
            for (int i = 0; i < images.length; i++) {
                images[i] = ImageIO.read(BulletOfGreen.class.getResource("/png/BulletOfStar" + i + ".png"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private boolean boom;

    public BulletOfStar(int x, int y, double angrad, int level, int canNotShot, int atk) {
        super(x, y, angrad, 20, 20, 25, level, canNotShot, atk);
        this.atk = atk;
        boom = false;
    }

    public BufferedImage getImage() {
        if (boom) {
            return null;
        }
        return images[level];

    }

    @Override
    public void step() {
        GameTools.getFlyingLine(this);
        flyDistance += speed;
        if (flyDistance >= canNotShot) {
            outOfDistance = true;
        } else {
            outOfDistance = false;
        }
    }

    @Override
    public void paint(Graphics g) {
        step();
        g.drawImage(getImage(), this.x, this.y, null);
    }

    public boolean isBoom() {
        return boom;
    }

    public void setBoom(boolean boom) {
        this.boom = boom;
    }

}

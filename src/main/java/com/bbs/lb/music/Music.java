package com.bbs.lb.music;

import javax.sound.sampled.*;
import java.io.IOException;

public class Music {
    byte[] data;
    AudioFormat format;
    int length;

    public void play() {

        Runnable runner = new Runnable() {
            @Override
            public void run() {
                AudioInputStream in;
                try {
                    in = AudioSystem.getAudioInputStream(
                            getClass().getResource("/music/BGMusic.wav"));
                    format = in.getFormat();
                    length = in.available();
                    data = new byte[length];
                    in.read(data);
                    in.close();
                    Clip clip = AudioSystem.getClip();
                    clip.open(format, data, 0, length);
                    clip.loop(length);
                } catch (UnsupportedAudioFileException | IOException e) {
                    e.printStackTrace();
                } catch (LineUnavailableException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(runner).start();
    }
}

package com.bbs.lb.background;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Logger;

public class BlueBackGround extends AbstractBackGround {
    private static final Logger LOGGER = Logger.getLogger(BlueBackGround.class.getName());
    private static BufferedImage image;

    static {
        try {
            image = ImageIO.read(Objects.requireNonNull(
                    BlueBackGround.class.getResource("/png/BG1.png")));
        } catch (IOException e) {
            LOGGER.severe("无法加载图像:" + e.getMessage());
        }
    }

    public void paint(Graphics g) {
        g.drawImage(image, x, y, null);
    }
}

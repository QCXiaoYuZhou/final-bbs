package com.bbs.lb.background;

import lombok.Getter;
import lombok.Setter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

@Getter
@Setter
public class Decorate extends AbstractCuteObject {
    private static BufferedImage[][] imagesBoom = new BufferedImage[5][6];

    static {
        for (int i = 0; i < imagesBoom.length; i++) {
            try {
                for (int j = 0; j < imagesBoom[i].length; j++) {
                    imagesBoom[i][i] = ImageIO.read(Objects.requireNonNull(
                            Decorate.class.getResource("/png/BulletBoom" + i + j + ".png")));
                }
            } catch (IOException e) {
                System.out.println("Decorate: " + e.getMessage());
            }
        }
    }

    private int index;
    private int towerNumber;

    public Decorate(int x, int y, int towerNumber) {
        super(x, y);
        index = -1;
        this.towerNumber = towerNumber;
    }

    @Override
    public BufferedImage getImage() {
        index++;
        if (index == imagesBoom[0].length - 1) {
            index = 5;
        }

        return imagesBoom[towerNumber][index];
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(getImage(), this.x, this.y, null);
    }
}

package com.bbs.lb.background;

import lombok.Data;

import java.awt.*;
import java.awt.image.BufferedImage;

@Data
public abstract class AbstractCuteObject {
    protected int x = 90;
    protected int y = 127;
    protected int width;
    protected int height;
    protected int ySpeed;
    protected int xSpeed;
    protected int speed;
    protected int HP;

    public AbstractCuteObject(int width, int height, int speed, int HP) {
        this.width = width;
        this.height = height;
        this.speed = speed;
        this.ySpeed = speed;
        this.xSpeed = 0;
        this.HP = HP;
    }

    public AbstractCuteObject(int width, int height, int HP, int xSpeed, int ySpeed) {
        this.width = width;
        this.height = height;
        this.ySpeed = ySpeed;
        this.xSpeed = xSpeed;
        this.HP = HP;
    }

    public AbstractCuteObject(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public abstract BufferedImage getImage();

    public void paint(Graphics g) {
        g.drawImage(getImage(), this.x, this.y, null);
    }

    public void move() {
        this.x += xSpeed;
        this.y += ySpeed;
    }
}

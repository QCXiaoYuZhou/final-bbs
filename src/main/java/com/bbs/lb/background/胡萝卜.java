package com.bbs.lb.background;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Logger;

public class 胡萝卜 extends AbstractCuteObject {
    private static final Logger 日志记录器 = Logger.getLogger(胡萝卜.class.getName());
    private static final BufferedImage[] 图像数组 = new BufferedImage[18];
    private static final int 图像索引偏移量 = 7;
    private static final int 图像数量 = 9;
    private static final double 胡萝卜索引增量 = 0.2;

    private double 胡萝卜索引 = 0;

    public 胡萝卜() {
        super(101, 75, 20, 0, 0);
        加载图像();
    }

    private void 加载图像() {
        for (int i = 0; i < 图像数组.length; i++) {
            try {
                图像数组[i] = ImageIO.read(Objects.requireNonNull(
                        Path.class.getResource("/png/hlb" + i + ".png")));
            } catch (IOException e) {
                日志记录器.severe("无法加载图像:" + e.getMessage());
            }
        }
    }

    @Override
    public BufferedImage getImage() {
        if (HP <= 0) {
            return null;
        }

        if (HP == 20) {
            胡萝卜索引 += 胡萝卜索引增量;
            return 图像数组[(int) 胡萝卜索引 % 图像数量 + 图像索引偏移量];
        }

        int 索引 = (HP - 1) / 3;
        日志记录器.info("索引: " + 索引);
        return 图像数组[索引];
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(getImage(), Path.SPECIAL_POINT[7][0], Path.SPECIAL_POINT[7][1], null);
    }
}

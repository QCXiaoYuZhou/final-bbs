package com.bbs.lb.background;

import com.bbs.lb.other.World;

public abstract class AbstractBackGround {
    protected int x;
    protected int y;
    protected int width;
    protected int height;

    public AbstractBackGround() {
        this.x = 0;
        this.y = 0;
        this.width = World.WIDTH;
        this.height = World.HEIGHT;
    }

    public AbstractBackGround(int y) {
        this.x = 0;
        this.y = y;
        this.width = World.WIDTH;
        this.height = World.HEIGHT;
    }
}

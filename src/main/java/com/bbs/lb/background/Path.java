package com.bbs.lb.background;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class Path extends AbstractBackGround {
    public static final int[][] SPECIAL_POINT = {
            {90, 115}, {90, 370}, {335, 370}, {335, 279}, {575, 279}, {575, 370}, {817, 370}, {800, 100}, {817, 110}
    };
    private static BufferedImage image;
    private static BufferedImage start;

    static {
        try {
            image = ImageIO.read(Objects.requireNonNull(Path.class.getResource("/png/Path.png")));
            start = ImageIO.read(Objects.requireNonNull(Path.class.getResource("/png/start01.png")));

        } catch (IOException e) {
            System.out.println("Path image not found");
        }

    }

    public Path() {
        super(120);
    }

    public void paint(Graphics g) {
        g.drawImage(image, x, y, null);
        g.drawImage(start, SPECIAL_POINT[0][0], SPECIAL_POINT[0][1], null);
    }
}

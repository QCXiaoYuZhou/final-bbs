package com.bbs.lb.background;

import lombok.Getter;
import lombok.Setter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

@Getter
@Setter
public class Enemy extends AbstractCuteObject {
    public static final int ENEMY_NUM = 15;
    public static int EnemyIndex;
    public static int HPs;
    public static int waveCount;
    public static int EnemyIndexForWaveCount;
    private static BufferedImage[][] images = new BufferedImage[11][2];
    private static int ImageIndex;
    private static BufferedImage coldImage;

    static {
        waveCount = 0;
        HPs = 0;
        EnemyIndex = 0;
        EnemyIndexForWaveCount = 0;
        try {
            for (int i = 0; i < images.length; i++) {
                images[i][0] = ImageIO.read(Objects.requireNonNull(Enemy.class.getResource("/png/landpink" + i + "_1.png")));
                images[i][1] = ImageIO.read(Objects.requireNonNull(Enemy.class.getResource("/png/landpink" + i + "_2.png")));
                coldImage = ImageIO.read(Objects.requireNonNull(Enemy.class.getResource("/png/cold.png")));
            }
        } catch (IOException e) {
            System.out.println("enemy image not found");
        }
    }

    private int imageIndex2;
    private boolean cold;
    private int index = 0;
    private int index1 = 0;

    public Enemy() {
        super(65, 65, 8, HPs);
        cold = false;
        EnemyIndex += 1;
        EnemyIndexForWaveCount += 1;
        ImageIndex = EnemyIndex / ENEMY_NUM;
        imageIndex2 = ImageIndex;
        waveCount = EnemyIndexForWaveCount / ENEMY_NUM + 1;

        final int x = 11;
        if (ImageIndex == x) {
            ImageIndex = 0;
            imageIndex2 = 0;
            EnemyIndex = 0;
        }

    }

    @Override
    public BufferedImage getImage() {
        if (this.HP > 0) {

            final int y = 15;
            if (index1++ % y == 0) {
                index++;
            }
            return images[imageIndex2][index % 2];

        }
        return null;
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(getImage(), this.x, this.y, null);
        if (cold) {
            g.drawImage(coldImage, this.x, this.y + 25, null);
        }
    }

    @Override
    public void setSpeed(int speed) {
        this.speed = speed;
        if (this.speed <= 0) {
            this.speed = 1;
        }
    }
}

package com.bbs.lb.tower;

import com.bbs.lb.bullet.AbstractBullets;
import com.bbs.lb.bullet.BulletOfCuttle;
import com.bbs.lb.other.World;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class CuttleTower extends AbstractTowers {

    protected static BufferedImage[] TowerImages = new BufferedImage[3];

    static {
        try {
            for (int i = 0; i < TowerImages.length; i++) {
                TowerImages[i] = ImageIO.read(GreenTower.class.getResource("/png/cuttle" + i + "0.png"));//读取炮塔图片
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    int index = 1;

    public CuttleTower(int x, int y) {
        super(x, y, 250, 40, 220, TowerImages);
    }

    @Override
    public AbstractBullets getNewBullet() {
        if (shootState) {
            if (index++ % 20 == 0) {
                return (new BulletOfCuttle(this.x, this.y, this.towerAngle, this.level, this.range, this.attack));
            }
        }
        return null;
    }

    @Override
    public void setLevel(int level) {
        World.score -= price;
        this.level = level;
        this.attack = level * 40 + 40;
    }
}

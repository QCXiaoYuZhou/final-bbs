package com.bbs.lb.tower;

import com.bbs.lb.bullet.AbstractBullets;
import com.bbs.lb.bullet.BulletOfGreen;
import com.bbs.lb.other.World;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class GreenTower extends AbstractTowers {
//=======
//绿头闪电炮塔
//public class GreenTower extends Towers{
//>>>>>>> c395a618f3269afd99f5d475d02e9af0223e39ba

    protected static BufferedImage[] TowerImages = new BufferedImage[3];

    static {
        try {
            for (int i = 0; i < TowerImages.length; i++) {
                TowerImages[i] = ImageIO.read(GreenTower.class.getResource("/png/Bottle" + i + "0.png"));//读取炮塔图片
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    final int temp1 = 1;
    int index = temp1;//控制子弹发射速度

    public GreenTower(int x, int y) {
        super(x, y, 300, 4, 100, TowerImages);//数字分别为射程、攻击力和价格
    }

    @Override
    public AbstractBullets getNewBullet() {
        if (shootState) {
            final int temp2 = 7;
            if (index++ % temp2 == 0) {
                return (new BulletOfGreen(this.x, this.y, this.towerAngle, this.level, this.range, this.attack));

            }
        }
        return null;
    }

    //炮塔升级
    @Override
    public void setLevel(int level) {
        World.score -= price;
        this.level = level;
        this.attack = level * 4 + 3;
    }
}

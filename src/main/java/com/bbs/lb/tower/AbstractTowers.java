package com.bbs.lb.tower;

import com.bbs.lb.bullet.AbstractBullets;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author gjj
 */
public abstract class AbstractTowers {

    private static BufferedImage TowerBase;

    static {
        try {
            TowerBase = ImageIO.read(BlueStarTower.class.getResource("/png/BStarBase.png"));//获取炮座
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    public int range;//射程
    protected int x;//x坐标
    protected int y;//y坐标
    protected int level;//炮塔等级
    protected int width;//炮塔宽度
    protected int height;//炮塔长度
    protected boolean shootState;//是否在发射，true为发射
    protected double towerAngle;//炮塔角度
    protected int attack;//攻击力
    protected int price;//价格
    //>>>>>>> c395a618f3269afd99f5d475d02e9af0223e39ba:Game/src/tower/Towers.java
    //=======
    protected BufferedImage[] TowerImages = new BufferedImage[3];//炮身
    //<<<<<<< HEAD:Game/src/tower/AbstractTowers.java
    private BufferedImage[] towerImages = new BufferedImage[3];//炮身

    public AbstractTowers(int x, int y, int range, int attack, int price, BufferedImage[] towerImages) {
        super();
        this.x = x;
        this.y = y;
        this.level = 0;
        this.width = 52;
        this.height = 52;
        this.range = range;
        this.shootState = false;
        this.attack = attack;
        this.price = price;
        this.towerImages = towerImages;
    }

    //炮塔随目标转动
    public void turnTowers(Graphics g) {
        g.drawImage(TowerBase, x, y + 5, null);
        Graphics2D g2d = (Graphics2D) g.create();
        g2d.rotate(-getTowerAngrad(), this.x + this.width / 2, this.y + this.height / 2);
        g2d.drawImage(getTowerImages(), this.x, this.y, null);
    }

    //返回炮身图片
    public BufferedImage getTowerImages() {
        return towerImages[level];
    }

    public AbstractBullets getNewBullet() {
        return null;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
        this.attack = level * 5 + 5;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getCanNotShot() {
        return range;
    }

    public void setCanNotShot(int range) {
        this.range = range;
    }

    public boolean isShootState() {
        return shootState;
    }

    public void setShootState(boolean shootState) {
        this.shootState = shootState;
    }

    public double getTowerAngrad() {
        return towerAngle;
    }

    public void setTowerAngle(double towerAngrad) {
        this.towerAngle = towerAngrad;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int atk) {
        attack = atk;
    }

    public int getPayFor() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}

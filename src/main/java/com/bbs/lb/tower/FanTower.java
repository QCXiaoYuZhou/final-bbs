package com.bbs.lb.tower;

import com.bbs.lb.bullet.AbstractBullets;
import com.bbs.lb.bullet.BulletOfFan;
import com.bbs.lb.other.World;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class FanTower extends AbstractTowers {
//=======
//风扇头炮塔
//public class FanTower extends Towers{
//>>>>>>> c395a618f3269afd99f5d475d02e9af0223e39ba

    protected static BufferedImage[] TowerImages = new BufferedImage[3];

    static {
        try {
            for (int i = 0; i < TowerImages.length; i++) {
                TowerImages[i] = ImageIO.read(GreenTower.class.getResource("/png/Fan" + i + "0.png"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    int index = 1;

    public FanTower(int x, int y) {
        super(x, y, 350, 1, 160, TowerImages);
    }

    @Override
    public AbstractBullets getNewBullet() {
        if (shootState) {
            final int temp = 12;
            if (index++ % temp == 0) {
                return (new BulletOfFan(this.x, this.y, this.towerAngle, this.level, this.range, this.attack));
            }
        }
        return null;
    }

    @Override
    public void setLevel(int level) {
        World.score -= price;
        this.level = level;
        this.attack = level + 2;
    }
}

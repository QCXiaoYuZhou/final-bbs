package com.bbs.lb.tower;

import com.bbs.lb.bullet.AbstractBullets;
import com.bbs.lb.bullet.BulletOfStar;
import com.bbs.lb.other.World;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class StarTower extends AbstractTowers {
    private static BufferedImage[] TowerImages = new BufferedImage[3];

    //=======
//星炮塔
//public class StarTower extends Towers{
//	protected static BufferedImage[] TowerImages=new BufferedImage[3];
//>>>>>>> c395a618f3269afd99f5d475d02e9af0223e39ba
    static {
        try {
            for (int i = 0; i < TowerImages.length; i++) {
                TowerImages[i] = ImageIO.read(GreenTower.class.getResource("/png/Star" + i + "0.png"));//读取炮塔图片
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    int index = 1;

    public StarTower(int x, int y) {
        super(x, y, 350, 3, 160, TowerImages);
    }

    @Override
    public AbstractBullets getNewBullet() {
        if (shootState) {
            final int temp = 15;
            if (index++ % temp == 0) {
                return (new BulletOfStar(this.x, this.y, this.towerAngle, this.level, this.range, this.attack));
            }
        }
        return null;
    }

    @Override
    public void setLevel(int level) {
        World.score -= price;
        this.level = level;
        this.attack = level * 3 + 3;
    }

}
